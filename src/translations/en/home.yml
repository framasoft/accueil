menu:
  actions: Our actions
  educ-pop: Popular education
  empowerment: Digital empowerment
  archipelago: Archipelago
  roadmap: Roadmap 2023-2025
  services: Open services
  findus: Finding us
  contact: Contact us
  mastodon: Mastodon (micro-blog)
  peertube: PeerTube (videos)
  mobilizon: Mobilizon (events)
  newsletter: Newsletter
  rss: RSS feed
support:
  md: |-
    If we can do all this, it is thanks to you, thanks to your donations!
  btn: Make a donation
intro:
  tube:
    label: To watch
  blog:
    label: To read
    read: Read more on the Framablog
prez:
  title: Did you say Framasoft?
  who: |-
    ### Who?

    A non-profit association founded in 2004, financed by your donations,
    which is limited to a dozen employees and about thirty volunteers (a group of friends!)
  what: |-
    ### What?

    - who [speaks](@:(link.tube)/c/framaconf/videos)<br />
    - that publishes [emancipatory content](@:(link.blog))<br />
    - that hosts [free services](@:(link.dio))<br />
    - that develops [free software](@:(link.soft)/empowerment#software)<br />
    - that cooperates [with others](@:(link.soft)/archipelago#partners)<br /> and
    - and which [federates Kittens](@:(link.kittens))
  why: |-
    ### Why?

    To contribute to a society of social justice where digital technology
    empowers people, against the backdrop of the imaginations of surveillance
    capitalism.
  title2: For whom?
  whom:
  - People who want to liberate their digital practices.
  - Actors of change and social justice.
  - The world of popular education that coordinates online.
known-for:
  title: We are known enough to…
  btn: Read the article on the Framablog
  try: Try it
  dio: Discover other services on Dégooglisons Internet
actions:
  title: We take concrete action to…
  btn: Read more
  empowerment: |-
    ### Digital empowerment

    **This is the first time that a group of people have been given the opportunity
    to participate in an online event.**

    Hosting free and open access online services.<br />
    → [Try our free services](https://degooglisons-internet.org)

    Development of free and federated software for empowerment.<br />
    → [Discover PeerTube](https://joinpeertube.org/)<br />
    → [Discover Mobilizon](https://joinmobilizon.org/)
  educ-pop: |-
    ### Popular education

    **Popular education is the freedom of everyone to share and access knowledge:
    the basis for a better world!**

    Sharing thoughts and initiatives, achievements on digital, commons and social
    justice.<br />
    → [Read the Framablog](@:link.blog)

    Sharing knowledge on digital empowerment in conferences and workshops.<br />
    → [See in videos](@:link.tube)
  archipelago: |-
    ### Archipelago

    **In an archipelago where each⋅e is an island (with its own identity) swimming
    in common waters (the same values), cooperating is a matter of course!**

    Animation of the CHATONS collective to facilitate access to other trusted hosts.<br />
    → [Find a CHATONS](@:link.chatons)

    Contributions to projects that drive us (experience sharing, technical
    support / communication, etc)<br />
    → [Know our partnerships](@:(link.soft)/archipelago)
fuel:
  title: 'Our fuel:<br />concrete action and the commons'
  desc: |-
    Here are some examples of our contributions to the cultural commons:
  list:
    - '[**Framalibre:**<br />the free directory](@:link.libre)'
    - '[**Books in the Commons:**<br />a publishing house that shakes up the codes](@:link.dlec)'
    - '[**Memo for (free) telework:**<br />sharing experiences](https://framasoft.frama.io/teletravail/)'
    - '[**[Resolved]:**<br />a guide to freeing up your digital uses](https://soyezresolu.org/)'
politic:
  title: Isn’t this a bit political?
  col:
    - |-
      At Framasoft, **we dream of a better world**. We don’t want to encourage
      a current system that is problematic (surveillance capitalism).
      We want to contribute to another system, which favours the commons.
      A path of hope, that is!

      → It’s political, because we are going **against the grain of a capitalist
      vision** where personal data is a source of enrichment for multinationals.
    - |-
      → It’s political because **preserving our freedoms is fundamental** to
      maintaining a not too shabby democracy.

      → It’s political because we want to **cooperate** with those who defend
      this vision of the world.

      We have expressed all this clearly and sincerely, in a manifesto:
  btn: Discover our manifesto
  quote: Giving everyone the power to act, through the power to understand!
clarification:
  title: 'Prejudices:<br />clarification'
  desc: |-
    “Framasoft must be neutral”: no! I’m not sure what the problem is,
    but I’m sure it’s a good one.

    “Framasoft should grow”: no more! We value our **small size** which allows
    us to have quality links between members.

    “Framasoft does a lot of things, so it has a lot of money”: not true!
    Our association **depends on you**, on your donations.

    The image of Framasoft is sometimes out of step with reality (it happens
    even to the best!).
    We take stock in a few minutes of video.
  quote: Accompanying those who want to change the world towards digital uses consistent with their values, that’s what we like!
  blog: |-
    We tell you more [on the Framablog](https://framablog.org/2021/11/16/frama-cest-aussi-des-contributions-dans-un-archipel/)
events:
  title: See you soon?
  subtitle: Here’s where we’ll be meeting soon…
  btn: Read more
  all: See the list of events where to find us on mobilizon.fr
