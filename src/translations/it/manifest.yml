humans:
  title: Agiamo per un mondo più dignitoso, costruito sulla condivisione, sulla solidarietà
    e sui beni comuni
  desc: "Siamo rivoltati dalle ingiustizie, le indegnità e le indecenze \ndel mondo\
    \ di oggi, plasmato dal capitalismo di sorveglianza.\nVogliamo contribuire all'emancipazione\
    \ delle persone attraverso il nostro campo d'azione \ne le nostre competenze:\
    \ i beni comuni digitali e culturali. \nCrediamo che gli esseri umani autonomi,\
    \ orgogliosi, consapevoli, solidali e disposti a condividere \npossano rendere\
    \ il mondo un posto migliore per tutti."
title: Manifesto
intro: "## Premessa\n\nNoi - i membri dell'associazione Framasoft - faremo del nostro\
  \ meglio per rispettare questo manifesto; \nsappiamo di essere troppo fallibili\
  \ per promettere di seguirlo parola per parola!\n\nConsiderate questo manifesto\
  \ come una semplice dichiarazione di intenti su ciò che vogliamo\nessere (e soprattutto\
  \ fare!) nel mondo."
empowerment:
  title: Sogniamo un mondo digitale liberato e facciamo del nostro meglio per realizzarlo
  desc: "In una cultura in cui la tecnologia digitale è onnipresente, vogliamo offrire\
    \ \nstrumenti facili da usare e liberatori, che diano potere alle persone rispettando\
    \ \nla loro integrità e dignità. \nTuttavia, accettiamo i nostri limiti quando\
    \ cerchiamo di realizzare questi ideali: \nfacciamo del nostro meglio e già non\
    \ è poco!"
experimentation:
  title: 'Sperimentiamo, anche se falliamo: in questo modo impariamo'
  desc: "Avendo più dubbi che certezze, vediamo ciò che facciamo come esperimenti.\
    \ \nAccettiamo quindi il fallimento: sbagliare è umano, e anche noi lo siamo!\
    \ \nTroviamo gioia persino nei fallimenti, perché possono insegnarci più di un\
    \ successo.\nMa per noi la conoscenza è più importante dell'efficienza."
action:
  title: Noi ci esprimiamo attraverso l'azione e la proposta
  desc: "Il nostro modo di esprimerci è fare. \nCi sentiamo utili nell'azione e crediamo\
    \ che i nostri modi di agire rivelino i nostri valori. \nVediamo Framasoft come\
    \ un'associazione di prefigurazione: presentiamo le nostre realizzazioni \ncome\
    \ prototipi di cui chiunque può appropriarsi per riprodurle su un'altra scala,\
    \ \no adattarle ad altri vincoli."
commons:
  title: Vogliamo condividere le nostre produzioni nei beni comuni
  desc: "Vogliamo fare in modo di condividere liberamente ricette, prodotti e lezioni\
    \ apprese. \nVogliamo portare le nostre conquiste tra i beni comuni. \nVogliamo\
    \ che tutti possano prenderle e adattarle alla propria situazione, \nma vogliamo\
    \ anche evitare di trovarci in una posizione di potere a causa del monopolio \n\
    su una risorsa o su un'altra."
compostability:
  title: Sappiamo che il mondo continuerà ad andare avanti anche dopo di noi
  desc: "Né noi né l'associazione siamo eterni... e lo accettiamo. \nAgiamo per la\
    \ compostabilità di Framasoft, cioè ci preoccupiamo \ndi lasciare tracce fertili,\
    \ di creare rovine utili perché chi verrà \ndopo di noi possa costruire con ciò\
    \ che resterà delle nostre azioni."
campfire:
  title: Vogliamo prenderci cura di noi stessi, degli altri e dei beni comuni che
    ci uniscono
  desc: "Questa affermazione ha i suoi limiti. \nAbbiamo capito che non possiamo prenderci\
    \ cura di un bene comune, né degli altri, se \nnon ci prendiamo cura di noi stessi,\
    \ così come non possiamo condividere una fiamma \nse lasciamo che il nostro fuoco\
    \ si spenga."
pathways:
  title: Rifiutiamo il potere di giudicare, legittimare o condannare
  desc: "Crediamo che tutti (compresi noi!) sono in un viaggio.\nPer questo motivo\
    \ cerchiamo di decostruire i riflessi puristi che ci porterebbero\na giudicare\
    \ moralmente le pratiche e i percorsi degli altri,\nsia per legittimarli che per\
    \ condannarli.\nCi preoccupiamo anche di non valorizzare le nostre modalità di\
    \ azione a scapito\ndi quelle adottate dai gruppi con cui condividiamo valori\
    \ ed ideali."
diversity:
  title: Ci sentiamo complementari ad altre iniziative
  desc: "Sappiamo di essere solo un pezzo del puzzle.\nQuello che facciamo sarà, inevitabilmente,\
    \ frammentario e insufficiente.\nÈ per questo che cerchiamo di instaurare legami\
    \ solidali con altri collettivi,\nche agiscono in altri contesti, con metodi propri.\n\
    Ai nostri occhi, la diversità delle modalità di azione è indispensabile per il\
    \ loro impatto."
fun:
  title: Non ci prendiamo sul serio
  desc: "Non vogliamo fare il passo più lungo della gamba.\nNon esitiamo a scherzare\
    \ per diminuire la pressione, anche se\nciò significa offendere chi è serio.\n\
    Se l'umorismo è al centro delle nostre pratiche, non toglie nulla\nalla sostanza\
    \ delle nostre azioni."
footer: 'Ultimo aggiornamento del manifesto: 27 ottobre 2022'
