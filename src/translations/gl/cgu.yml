title: Condicións xerais de uso
intro: "## Preámbulo\n\nAo usar este servizo, aceptas estar obrigado polas seguintes
  condicións.<br />\n@:color.soft resérvase o dereito de actualizar e modificar estas
  condicións\ncandoo considere necesario.\n\n## Versión curta\n\n([TL;DR](https://en.wiktionary.org/wiki/tl;dr)
  como dicimos aquí :wink:)<br />\nPara facilitar a lectura, sen o latinorio legal,
  ofrecémosche a continuación unha\nversión comprensíbel por un ser humano normal."
short:
  sympa:
    text: 'Somos boa xente:'
    list:
    - O teu contido perténceche a ti (mais recomendámosche que o publiques baixo unha
      licenza libre);
    - '@:color.soft non empregará os teus datos persoais, agás para estatísticas internas
      (anónimas) ou para notificarche un cambio importante no servizo;'
    - '@:color.soft non transmitirá nin revenderá os teus datos persoais (a túa privacidade
      é realmente importante para nós);'
    - Os compromisos de @:color.soft resultan dun enfoque activista inspirado no movemento
      de educación popular, podes atopalos na nosa [Declaración de servizos libres,
      éticos, descentralizados e solidarios](../charte).
  terms:
    text: '**Porén** iso non significa que debas pensar que somos uns papáns:'
    list:
    - 'Cláusula *«A lei é a lei, e nós non queremos terminar no cárcere»*: debes respectar
      a lei (ben feita ou estúpida), se non, a túa conta será eliminada;'
    - 'Cláusula *«Grazas por ser educado e respectuoso cos seus veciños de servizo»*:
      debes respectar aos demais usuarios amosando civismo e educación, se non, o
      teu contido, ou mesmo a túa conta, pode ser eliminada sen negociación;'
    - 'Cláusula *«Se creba, non estamos obrigados a reparar»*: @:color.soft ofrece
      este servizo de xeito libre e de balde. Se perdes datos, sexa por culpa túa
      ou nosa, desculpa, mais pasa. Faremos o posíbel para recuperalos, mais non asumimos
      ningunha obriga de resultados. Evidentemente, evita poñer datos sensíbeis ou
      importantes nos servizos @:color.soft, porque en caso de perda, non garantimos
      a súa recuperación;'
    - 'Cláusula *«Se non estás satisfeito, convidámoste a buscar noutro lugar»*: se
      o servizo non che convén, podes buscar un equivalente (ou mellor) noutro lugar,
      ou montar o teu;'
    - 'Cláusula *«Calquera abuso será sancionado»*: se un usuario abusa do servizo,
      por exemplo monopolizando recursos de máquinas compartidas, ou publicando contidos
      considerados irrelevantes, o seu contido ou conta pode ser eliminado sen previo
      aviso nin negociación. @:color.soft segue a ser o único xuíz desta noción de
      «abuso» para ofrecer o mellor servizo posíbel a todos os seus usuarios. Se isto
      che parece antidemocrático, antiliberador e antiliberdade de expresión, consulta
      a cláusula anterior;'
    - 'Cláusula *«Nada é eterno»*: os servizos poden pechar (por falta de fondos para
      mantelos, por exemplo), poden ser vítimas de intrusión («non existe o 100% seguro»).
      Polo tanto, recomendámosche que gardes unha copia dos datos que sexan importantes
      para ti, porque @:color.soft non se fai responsable da súa hospedaxe sen límite
      de tempo.'
footer: "Condicións xerais de uso dos servizos en liña da asociación @:color.soft,<br
  />versión\n1.0.3 do 11 de abril de 2016."
full: "## Versión completa\n### Condicións do servizo\n\n1. O uso do servizo é baixo
  o seu propio risco. O servizo fornécese tal e como está.\n2. Non debe modificar
  outro sitio para indicar falsamente que está asociado con\n  este servizo @:(color.soft).\n
  3. As contas só poden ser creadas e usadas por humanos. As contas creadas por\n\
  \  robots ou outros métodos automatizados pódense eliminar sen previo aviso.\n4.
  Vostede é responsábel da seguridade da súa conta e contrasinal.\n  @:color.soft
  non pode nin será responsábel de ningunha perda ou dano\n  derivado do incumprimento
  desta obriga de seguridade.\n5. Vostede é o responsábel de todo o contido publicado
  e da actividade que\n  se produza na súa conta.\n6. Non pode usar o Servizo con
  fins ilegais ou non autorizados.\n  Non debes transgredir as leis do seu país.\n
  7. Non pode vender, comerciar, revender ou explotar con fins comerciais\n  non autorizados
  ningunha conta do servizo utilizada.\n\nA violación de calquera destes acordos dará
  lugar á cancelación da súa conta.\nVostede entende e acepta que a asociación @:color.soft
  non se fai responsábel\ndo contido publicado neste servizo.\n\n1. Vostede entendes
  que poñer o servizo e o seu contido en liña implica a transmisión\n  (en claro ou
  cifrada, dependendo do servizo) a través de varias redes.\n2. Non debe transmitir
  vermes, virus ou calquera outro código de natureza maliciosa.\n3. @:color.soft non
  garante que\n  - o servizo responderá ás súas necesidades específicas,\n  - o servizo
  será ininterrompido ou libre de erros,\n  - se corrixirán os erros no servizo.\n
  4. Vostede entende e acepta que @:color.soft non se fai responsábel de ningún dano\n
  directo, indirecto ou incidental, incluídos os danos por lucro cesante, clientes,
  acceso,\ndatos ou outras perdas intanxibeis (aínda que @:color.soft fose informado
  da\nposibilidade de tales danos) e que resultarían de:\n  1. o uso ou a imposibilidade
  de utilizar o servizo;\n  2. acceso non autorizado ou alteración da transmisión
  de datos;\n  3. declaracións ou accións dun terceiro sobre o servizo;\n  4. rescisión
  da súa conta;\n  5. calquera outra cuestión relativa ao servizo.\n5. O fracaso de
  @:color.soft para exercer ou facer valer calquera dereito ou\n  disposición das
  Condicións xerais de uso non constituirá unha renuncia a\n  ese dereito ou disposición.
  As Condicións xerais de uso constitúen o acordo\n  completo entre vostede e @:color.soft
  e rexen o seu uso do servizo,\n  substituíndo calquera acordo anterior entre vostede
  e   @:color.soft\n  (incluídas as versións anteriores das Condicións xerais de uso).\n
  6. As preguntas sobre as condicións do servizo deben enviarse a través\n  deste
  [formulario de contacto](@:link.contact).\n\n## Modificacións no servizo\n\n1. @:color.soft
  resérvase o dereito, en calquera momento, de modificar ou\n  interromper, temporal
  ou permanentemente, o servizo con ou sen previo aviso.\n2. @:color.soft non será
  responsábel ante vostede nin ningún terceiro por\n  calquera modificación, suspensión
  ou interrupción do Servizo.\n\n### Dereitos de autoría do contido\n\n1. Non pode
  enviar, cargar, bloguear, distribuír, difundir ningún contido que sexa ilícito,\n\
  \  difamatorio, acosador, abusivo, fraudulento, infractor, obsceno ou censurábel.\n
  2. Non reclamamos ningún dereito sobre os seus datos: textos, imaxes, son, vídeo
  ou\n  calquera outro elemento, que descargue ou transmita desde a súa conta.\n3.
  Non usaremos o seu contido para outro propósito que non sexa fornecerlle\n  o servizo.\n
  4. Non debe cargar nin poñer a disposición ningún contido que infrinxa os dereitos\n\
  \  de ninguén.\n5. Reservámonos o dereito de eliminar calquera contido que consideremos
  irrelevante\n  para o uso do servizo, segundo o noso criterio exclusivo.\n6. Podemos,
  se é necesario, eliminar ou impedir a distribución de calquera contido\n  do servizo
  que non cumpra estas condicións.\n\n### Edición e intercambio de datos\n\n- Os ficheiros
  que cree co servizo poderán ser -se así o quere- lidos, copiados,\n  empregados
  e redistribuídos por persoas que vostede coñeza ou non.\n- Ao facer públicos os
  seus, recoñece e acepte que calquera persoa que empregue\n  este sitio web pode
  consultalos sen restricións.\n- Mais o servizo pode tamén ofrecerlle a posibilidade
  de autorizar o acceso e o\n  traballo colaborativo nos seus documentos de xeito
  restrinxido a un ou máis usuarios.\n- @:color.soft non se fai responsábel de ningún
  problema resultante do intercambio\n  ou da publicación de datos entre os usuarios.\n
  \n### Peche\n\n@:color.soft, ao seu exclusivo criterio, ten dereito a suspender
  ou cancelar a súa\nconta e rexeitar calquera uso actual ou futuro do servizo. A
  terminación do\nservizo dará lugar á desactivación do acceso á súa conta e á restitución
  de\ntodos os contidos.<br /> @:color.soft resérvase o dereito de rexeitar o servizo\n
  a calquera persoa por calquera motivo en calquera momento.\n\n@:color.soft tamén
  se reserva o dereito de cancelar a súa conta se non inicia\nsesión na súa conta
  durante un período superior a 6 meses.\n\n### Información persoal\n\nDe conformidade
  co artigo 34 da lei «Informatique et Libertés», @:color.soft\ngarante ao usuario
  o dereito de oposición, acceso e rectificación sobre os\ndatos persoais que o conciernen.
  O usuario ten a posibilidade de exercer este\ndereito mediante o [formulario de
  contacto](@:link.contact) dispoñíbel.\n\n- Para usar certos servizos @:color.soft,
  debe crear unha conta. @:color.soft\n  solicita información persoal: un enderezo
  de correo-e válido e un contrasinal\n  que se emprega para protexer a súa conta
  contra o acceso non autorizado.\n  Os campos «Apelidos» e «Nome» poden ser necesarios
  para o correcto \n funcionamento do software, mais non é necesario que revelen a
  súa verdadeira identidade.\n- Do mesmo xeito que outros servizos en liña, @:color.soft
  garda automaticamente\n  certa información sobre o seu uso do servizo, como a actividade
  da conta\n  (p. ex.: espazo de almacenamento ocupado, número de entradas, medidas
  tomadas),\n  os datos amosados ou nos que se preme (p. ex.emplo: ligazóns, elementos
  da interface\n  de usuario) e outra información para identificalo a Vde. (p. ex.:
  tipo de navegador,\n  enderezo IP, data e hora de acceso, URL de referencia).\n
  - Usamos esta información internamente para mellorar para Vde. a interfacede\n \
  \ usuario dos servizos @:color.soft e manter unha experiencia de usuario consistente
  e fiábel.\n- Estes datos non se venden nin se ceden a terceiros."
